# /usr/bin/env python3

# Author : Jalright
# Date : 2020-03-11

import xlrd
import sys
import xlwt
import time


def get_duplicate_record_rows(filename, start_column=0, end_column=-1):
    """get the duplicate rows number"""
    data = xlrd.open_workbook(filename)
    table = data.sheets()[0]
    nrows = table.nrows
    lines_dict = {}
    for i in range(nrows):
        # get the compare list to string
        # print(str(table.row_values(i)))
        if end_column == -1:
            line = str(table.row_values(i)[start_column:])
        else:
            line = str(table.row_values(i)[start_column:end_column])
        # print(line)
        # try to add the count of the string dictionary
        try:
            # if the string is in the dictionary , then add the value list column[0]
            lines_dict[line][0] = lines_dict[line][0] + 1
            # append the row number to the value list
            lines_dict[line].append(i)
        except Exception as e:
            # print(str(e))
            # if the string is not in the dictionary , then crerate and add the value list column[0]
            lines_dict[line] = []
            lines_dict[line].append(1)
            # append the row number to the value list
            lines_dict[line].append(i)
    # print(lines_dict)
    # if the record have duplicate keys the add the row number to the dup_list
    dup_list = []
    for k, v in lines_dict.items():
        if v[0] > 1:
            # print("----")
            #print('%s ,duplicate row ：%s' % (k, str(v[1:])))
            dup_list = dup_list + v[1:]
    return dup_list


def wirte_excel(dup_list):
    """write all record the result_*.xls  with unique and duplicate sheets"""
    style0 = xlwt.easyxf(
        'font: name Times New Roman, color-index black, bold off')
    wb = xlwt.Workbook()
    ws1 = wb.add_sheet('唯一')
    ws2 = wb.add_sheet('重复')
    data = xlrd.open_workbook(filename)
    table = data.sheets()[0]
    nrows = table.nrows
    liens = []
    row1 = 0
    row2 = 0
    for i in range(nrows):
        # write the duplicate records
        if i in dup_list:
            col = 0
            line = table.row_values(i)
            # traverse the line get columns the the row
            for item in line:
                ws2.write(row2, col, item, style0)
                col = col + 1
            row2 = row2 + 1
        # write the unique records
        else:
            col = 0
            line = table.row_values(i)
            for item in line:
                ws1.write(row1, col, item, style0)
                col = col + 1
            row1 = row1 + 1
    # save the result
    wb.save("unique_duplicate_%s.xls" % time.strftime("%Y%m%d%H%M%S"))


if __name__ == "__main__":
    args = sys.argv
    # show help
    if len(args) > 1 and args[1] == '--help':
        print(args[0] + " 文件名  [[开始列] 结束列]\n默认是对比所有的列\n 如果只指定开始列，就对比开始列到最后一列的数据")
        sys.exit()
    if len(args) > 3:
        filename = args[1]
        start_column = int(args[2]) - 1
        end_column = int(args[3])
        rows_list = get_duplicate_record_rows(
            filename=filename, start_column=start_column, end_column=end_column)
        # print(rows_list)
        wirte_excel(rows_list)
        sys.exit()
    if len(args) > 2:
        filename = args[1]
        start_column = int(args[2]) - 1
        rows_list = get_duplicate_record_rows(
            filename=filename, start_column=start_column, end_column=-1)
        # print(rows_list)
        wirte_excel(rows_list)
        sys.exit()
    if len(args) > 1:
        filename = args[1]
        rows_list = get_duplicate_record_rows(filename)
        print(rows_list)
        wirte_excel(rows_list)
        sys.exit()

    print(args[0] + " 文件名  [[开始列] 结束列]\n默认是对比所有的列\n 如果只指定开始列，就对比开始列到最后一列的数据")
