# windows 常用脚本

## 图标更新

桌面或者资源管理器的图标发现没有刷新，可以使用脚本进行刷新

``` bat
flush_icon.bat
```

## 磁盘碎片整理

``` bat
defrag.bat
```
